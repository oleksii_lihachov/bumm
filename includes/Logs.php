<?php

namespace Buum;

class Logs {
	public $wpdb;
	public $table_name;

	public function __construct() {
		global $wpdb;
		$this->wpdb       = &$wpdb;
		$this->table_name = $wpdb->prefix . "buum_logs";

		$this->init();
	}

	private function init() {
		$charset_collate    = $this->wpdb->get_charset_collate();
		$search_table_query = $this->wpdb->prepare( 'SHOW TABLES LIKE %s', $this->wpdb->esc_like( $this->table_name ) );

		if ( $this->wpdb->get_var( $search_table_query ) === $this->table_name ) {
			return false;
		}

		$sql = "CREATE TABLE {$this->table_name} (
			   `log_id` INT NOT NULL AUTO_INCREMENT,
				`buum_sku` INT NOT NULL,
  		  	    `import_date` DATETIME NOT NULL DEFAULT now(),
			    `product` VARCHAR(255) NOT NULL,
  			     `action` VARCHAR(255) NULL,
  				 UNIQUE INDEX `log_id_UNIQUE` (`log_id` ASC),
  				 PRIMARY KEY (`log_id`)) {$charset_collate};";

		require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
		dbDelta( $sql );

		return true;
	}

	public function insert( $buum_sku, $product_name, $action ) {
		$this->wpdb->insert(
			$this->table_name,
			array(
				'buum_sku'    => $buum_sku,
				'import_date' => date( 'Y-m-d H:i:s' ),
				'product'     => $product_name,
				'action'      => $action
			)
		);
	}

	public function clear() {
		$this->wpdb->query( "TRUNCATE {$this->table_name}" );
	}
}