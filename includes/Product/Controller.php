<?php

namespace Buum\Product;

class Controller {
	private $product;
	private $data;
	static $categories_ids = array();

	public function __construct( \WC_Product $product, $data ) {
		$this->product = $product;
		$this->data    = $data;
	}

	static function get_raw_attributes( $data ) {
		$buum_product_settings = get_option( 'buum_product' );
		$attributes            = array();

		if ( $buum_product_settings['import_color'] && ! empty( $data['COLORNAME'] ) ) {
			$attributes['color'] = $data['COLORNAME'];
		}

		if ( $buum_product_settings['import_producer'] && ! empty( $data['PRODUCERNAME'] ) ) {
			$attributes['producer'] = $data['PRODUCERNAME'];
		}

		if ( $buum_product_settings['import_size'] && ! empty( $data['SIZENAME'] ) ) {
			$attributes['size'] = $data['SIZENAME'];
		}

		return $attributes;
	}

	static function create_product_post( $data, $execute_variation ) {
		$id     = $data['ARTNO'];
		$result = null;

		$posts = get_posts( array(
			'post_type'      => 'product',
			'meta_query'     => array(
				array(
					'key'   => '_buum_id',
					'value' => $id,
				)
			),
			'posts_per_page' => 2,
			'post_status'    => array( 'trash', 'publish' )
		) );

		if ( empty( $posts ) ) {
			if ( $data['EXPORTABLE'] !== 'T' ) {
				return null;
			}

			if ( $execute_variation && ! empty( $data['OWNERNO'] ) ) {
				return null;
			}

			if ( ! function_exists( 'post_exists' ) ) {
				require_once( ABSPATH . 'wp-admin/includes/post.php' );
			}

			if ( $post_id = post_exists( $data['product_name'] ) ) {
				update_post_meta( $post_id, '_buum_id', $id );

				$result['action']     = 'Updated Existing product';
				$result['product_id'] = $post_id;
			} else {
				$post = array(
					'post_title'  => $data['product_name'],
					'post_status' => "publish",
					'post_type'   => "product"
				);

				$new_post_id = wp_insert_post( $post );

				if ( is_wp_error( $new_post_id ) ) {
					return null;
				}

				update_post_meta( $new_post_id, '_buum_id', $id );
				$result['action']     = 'Imported';
				$result['product_id'] = $new_post_id;
			}
		} else {
			wp_update_post( array(
				'ID'          => $posts[0]->ID,
				'post_title'  => $data['product_name'],
				'post_status' => $data['EXPORTABLE'] == 'T' ? 'publish' : 'trash',
			) );

			$result['action']     = 'Updated';
			$result['product_id'] = $posts[0]->ID;
		}

		return $result;
	}

	static function get_product_type( $data, $connect, $langNo, $defaultPriceListNo, $stock ) {
		if ( ! empty( $data['OWNERNO'] ) ) {
			return 'variation';
		}

		// Single Variable
		if ( ! empty( self::get_raw_attributes( $data ) ) ) {
			return 'variable';
		}

		// Have child items
		if ( (int) $data['ARTTYPE'] === 3 ) {
			$is_empty_attrs = false;

			$variations = $connect->getProductsByLanguage(
				$langNo,
				$defaultPriceListNo,
				array(
					'variations' => $data['ARTNO'],
					'warehouses' => (array) $stock['warehouses'],
				)
			);

			foreach ( $variations as $variation ) {
				if ( empty( self::get_raw_attributes( $variation ) ) ) {
					$is_empty_attrs = true;
					break;
				}
			}

			return $is_empty_attrs ? 'grouped' : 'variable';
		}

		// Variable with BARCODE variations
		$additional_variations = $connect->getBarcodeProduct( $data['ARTNO'], $stock['warehouses'] );
		if ( ! empty( $additional_variations ) ) {
			return 'variable';
		}

		return 'simple';
	}

	public function process_general_info() {
		update_post_meta( $this->product->get_id(), '_sku', $this->data['ARTCODE'] );
		$this->product->set_regular_price( $this->data['PRICE'] );
		$this->product->set_price( $this->data['PRICE'] );
		$this->product->set_manage_stock( true );
		$this->product->set_stock_quantity( $this->data['STOCK_QTY'] );
		$this->product->set_weight( $this->data['WEIGHT'] );
		$this->product->set_length( $this->data['LEN'] );
		$this->product->set_height( $this->data['HEIGHT'] );
		$this->product->set_width( $this->data['WIDTH'] );
	}

	public function process_attributes( $parent_id, $for_variation, &$product_attributes ) {
		$attributes = self::get_raw_attributes( $this->data );

		foreach ( $attributes as $attribute => $term_name ) {
			$taxonomy = 'pa_' . $attribute; // The attribute taxonomy

			if ( ! taxonomy_exists( $taxonomy ) ) {
				continue;
			}

			// Check if the Term name exist and if not we create it.
			if ( ! term_exists( $term_name, $taxonomy ) ) {
				wp_insert_term( $term_name, $taxonomy ); // Create the term
			}

			$term_slug = get_term_by( 'name', $term_name, $taxonomy )->slug; // Get the term slug

			// Get the post Terms names from the parent variable product.
			$post_term_names = wp_get_post_terms( $parent_id, $taxonomy, array( 'fields' => 'names' ) );

			// Check if the post term exist and if not we set it in the parent variable product.
			if ( ! in_array( $term_name, $post_term_names ) ) {
				wp_set_post_terms( $parent_id, $term_name, $taxonomy, true );
			}

			// Set/save the attribute data in the product variation
			$updated_id = $for_variation ? $this->product->get_id() : $parent_id;
			update_post_meta( $updated_id, 'attribute_' . $taxonomy, $term_slug );

			// Update product attributes
			$product_attributes[ $taxonomy ] = array(
				'name'         => $taxonomy,
				'value'        => '',
				'position'     => '',
				'is_visible'   => 0,
				'is_variation' => 1,
				'is_taxonomy'  => 1
			);
		}
	}

	public function process_tags() {
		$tags = array();

		for ( $nr = 1; $nr <= 6; $nr ++ ) {
			if ( ! empty( $this->data[ 'INFO' . $nr ] ) ) {
				$tags[] = $this->data[ 'INFO' . $nr ];
			}
		}

		wp_set_object_terms( $this->product->get_id(), $tags, 'product_tag' );
	}

	public function process_categories() {
		if ( empty( self::$categories_ids ) ) {
			return;
		}
		list( $buum_ids, $map_ids ) = self::$categories_ids;

		if ( isset( $buum_ids[ $this->data['GROUPNO'] ] ) ) {
			wp_set_object_terms( $this->product->get_id(), $buum_ids[ $this->data['GROUPNO'] ]['term_id'], 'product_cat', false );
		}
		if ( isset( $map_ids[ $this->data['GROUPNO'] ] ) ) {
			wp_set_object_terms( $this->product->get_id(), $map_ids[ $this->data['GROUPNO'] ], 'product_cat', false );
		}
	}

	public function process_related_products() {
		$relatedIds = array();

		foreach ( explode( ',', $this->data['related_articles'] ) as $relatedArtNo ) {
			$relatedArtNo = trim( $relatedArtNo );
			$relatedArtNo = (int) $relatedArtNo;
			if ( empty( $relatedArtNo ) ) {
				continue;
			}
			$wooRelatedProducts = get_posts( array(
				'post_type'      => 'product',
				'meta_query'     => array(
					array(
						'key'   => '_buum_id',
						'value' => $relatedArtNo,
					)
				),
				'posts_per_page' => 2,
				'post_status'    => array( 'trash', 'publish' )
			) );

			if ( ! empty( $wooRelatedProducts ) ) {
				foreach ( $wooRelatedProducts as $relatedProd ) {
					$relatedIds[] = $relatedProd->ID;
				}
			}

		}

		$this->product->set_cross_sell_ids( $relatedIds );
	}

	public function process_description() {
		if ( ! empty( $this->data['product_desc'] ) ) {
			$this->product->set_description( $this->data['product_desc'] );
//			$this->product->set_short_description( $this->data['product_desc'] );
		}
	}

	public function sync_artno_products() {
		$this->product->set_regular_price( $this->data['PRICE'] );
		$this->product->set_price( $this->data['PRICE'] );
		$this->product->set_manage_stock( true );
		$this->product->set_stock_quantity( $this->data['STOCK_QTY'] );
		$this->product->save();
	}
}