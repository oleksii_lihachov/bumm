<?php

namespace Buum\Product;

class Grouped {
	public function __construct( $info, $data, $connect, $langNo, $defaultPriceListNo, $stock, \Buum\Logs $logs ) {
		//make product type be grouped:
		wp_remove_object_terms( (int) $info['product_id'], 'simple', 'product_type' );
		wp_remove_object_terms( (int) $info['product_id'], 'variable', 'product_type' );
		wp_set_object_terms( (int) $info['product_id'], 'grouped', 'product_type', false );

		$product = wc_get_product( $info['product_id'] );

		$controller = new Controller( $product, $data );
		$child_ids  = array();

		$children = $connect->getProductsByLanguage(
			$langNo,
			$defaultPriceListNo,
			array(
				'variations' => $data['ARTNO'],
				'warehouses' => (array) $stock['warehouses'],
			)
		);

		if ( ! empty( $children ) ) {
			foreach ( $children as $child ) {
				$child_info = $this->create_child( $child );

				if ( ! $child_info ) {
					continue;
				}

				$child_ids[]      = $child_info['product_id'];
				$child_product    = wc_get_product( $child_info['product_id'] );
				$child_controller = new Controller( $child_product, $child );

				$child_controller->process_general_info();
				$child_controller->process_tags();
				$child_controller->process_related_products();
				$child_controller->process_categories();
				$child_controller->process_description();

				$child_product->save();

				$logs->insert( $child['ARTCODE'], $child['product_name'], $child_info['action'] . ' (Simple, child of Grouped product: ' . $data['product_name'] . ')' );
			}
		}

		if ( ! empty( $child_ids ) ) {
			$product->set_children( $child_ids );
		}

		update_post_meta( $info['product_id'], '_sku', $data['ARTCODE'] );

		$controller->process_tags();
		$controller->process_related_products();
		$controller->process_categories();
		$controller->process_description();
		$product->save();

		$logs->insert( $data['ARTCODE'], $data['product_name'], $info['action'] . ' (Grouped)' );
	}

	private function create_child( $data ) {
		return Controller::create_product_post( $data, false );
	}
}