<?php

namespace Buum\Product;

class Simple {
	public function __construct( $info, $data, \Buum\Logs $logs ) {
		wp_remove_object_terms( (int) $info['product_id'], 'grouped', 'product_type' );
		wp_remove_object_terms( (int) $info['product_id'], 'variable', 'product_type' );

		$product    = wc_get_product( $info['product_id'] );
		$controller = new Controller( $product, $data );

		$controller->process_general_info();
		$controller->process_tags();
		$controller->process_related_products();
		$controller->process_categories();
		$controller->process_description();

		$product->save();

		$logs->insert( $data['ARTCODE'], $data['product_name'], $info['action'] . ' (Simple)' );
	}
}