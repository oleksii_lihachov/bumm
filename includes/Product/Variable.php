<?php

namespace Buum\Product;

class Variable {
	public function __construct( $info, $data, $connect, $langNo, $defaultPriceListNo, $stock, \Buum\Logs $logs ) {
		//make product type be variable:
		wp_remove_object_terms( (int) $info['product_id'], 'simple', 'product_type' );
		wp_remove_object_terms( (int) $info['product_id'], 'grouped', 'product_type' );
		wp_set_object_terms( (int) $info['product_id'], 'variable', 'product_type', false );

		$product = wc_get_product( $info['product_id'] );

		$attributes = array();
		$controller = new Controller( $product, $data );
		$is_set_sku = true;

		$variations = $connect->getProductsByLanguage(
			$langNo,
			$defaultPriceListNo,
			array(
				'variations' => $data['ARTNO'],
				'warehouses' => (array) $stock['warehouses'],
			)
		);

		// Additional variation by parent product
		$additional_variations = $connect->getBarcodeProduct(
			$data['ARTNO'],
			$stock['warehouses']
		);

		// Check if single variable
		if ( ! empty( Controller::get_raw_attributes( $data ) ) ) {
			$is_set_sku = false;

			if ( empty( $additional_variations ) && (int) $data['ARTTYPE'] !== 3 ) {
				new Variation( $product, $data, $attributes, $logs );
			} else {
				$sizes = $this->process_barcode_variation( $additional_variations, $data, $connect, $product, $logs, $attributes );

				// Create main variation
				if ( ! empty( $data['SIZENAME'] ) && ! in_array( $data['SIZENAME'], $sizes ) ) {
					new Variation( $product, $data, $attributes, $logs );
				}
			}
		}

		if ( ! empty( $variations ) ) {
			foreach ( $variations as $variation ) {
				$additional_variations = $connect->getBarcodeProduct(
					$variation['ARTNO'],
					$stock['warehouses']
				);

				if ( empty( $additional_variations ) ) {
					new Variation( $product, $variation, $attributes, $logs );
				} else {
					// Prevent creation variation with just one SIZE attribute without color
					if ( empty( $variation['COLORNAME'] ) ) {
						continue;
					}

					$sizes = $this->process_barcode_variation( $additional_variations, $variation, $connect, $product, $logs, $attributes );

					// Create main variation
					if ( ! empty( $variation['SIZENAME'] ) && ! in_array( $variation['SIZENAME'], $sizes ) ) {
						new Variation( $product, $variation, $attributes, $logs );
					}
				}
			}
		}

		if ( $is_set_sku ) {
			update_post_meta( $info['product_id'], '_sku', $data['ARTCODE'] );
		}


		update_post_meta( $info['product_id'], '_product_attributes', $attributes );

		$controller->process_tags();
		$controller->process_related_products();
		$controller->process_categories();
		$controller->process_description();
		$product->save();

		$logs->insert( $data['ARTCODE'], $data['product_name'], $info['action'] . ' (Variable)' );
	}

	public function process_barcode_variation( $items, $default_data, $connect, $product, $logs, &$attributes ) {
		$sizes = array();

		foreach ( $items as $item ) {
			$size_attribute = $connect->getSizeById( $item['SIZENO'] );

			// Prevent creation variation if not found SIZE attribute in DB
			if ( empty( $size_attribute ) ) {
				continue;
			}

			$data              = $default_data;
			$data['ARTCODE']   = $item['BARCODE'];
			$data['STOCK_QTY'] = $item['STOCK_QTY'];
			$data['SIZENO']    = $item['SIZENO'];
			$data['SIZENAME']  = $size_attribute['SIZENAME'];

			new Variation( $product, $data, $attributes, $logs );

			$sizes[] = $size_attribute['SIZENAME'];
		}

		return $sizes;
	}
}