<?php

namespace Buum\Product;

use WC_Product_Variation;
use WC_Product;

class Variation {
	private $data;
	private $parent;

	public function __construct( WC_Product $parent, $data, &$attributes, \Buum\Logs $logs ) {
		$this->data   = $data;
		$this->parent = $parent;
		$info         = $this->create();
		$controller   = new Controller( $info['variation'], $data );

		$controller->process_attributes( $parent->get_id(), true, $attributes );
		$controller->process_general_info();
		$controller->process_description();
		$info['variation']->save();

		$logs->insert( $data['ARTCODE'], $data['product_name'], $info['action'] . ' (Variation of parent product: ' . $parent->get_name() . ')' );
	}

	private function create() {
		$info         = null;
		$variation_id = $this->data['ARTCODE'];

		$variation_posts = get_posts( array(
			'post_type'   => 'product_variation',
			'meta_query'  => array(
				array(
					'key'   => '_buum_variation_id',
					'value' => $variation_id,
				)
			),
			'post_status' => array( 'publish' )
		) );

		if ( empty( $variation_posts ) ) {
			$variation_post = array(
				'post_title'  => $this->parent->get_title(),
				'post_name'   => 'product-' . $this->data['ARTNO'] . '-variation',
				'post_status' => 'publish',
				'post_parent' => $this->parent->get_id(),
				'post_type'   => 'product_variation',
				'guid'        => $this->parent->get_permalink()
			);

			$variation_id = wp_insert_post( $variation_post );
			update_post_meta( $variation_id, '_buum_id', $this->data['ARTNO'] );
			update_post_meta( $variation_id, '_buum_variation_id', $this->data['ARTCODE'] );

			$info['action']    = 'Imported';
			$info['variation'] = new WC_Product_Variation( $variation_id );

			if ( ! empty( $this->data['SIZENO'] ) ) {
				update_post_meta( $variation_id, '_buum_variation_sizeno', $this->data['SIZENO'] );
				$info['action'] = 'Imported from BARCODE table';
			}
		} else {
			wp_update_post(
				array(
					'ID'          => $variation_posts[0]->ID,
					'post_parent' => $this->parent->get_id(),
				)
			);
			update_post_meta( $variation_posts[0]->ID, '_buum_id', $this->data['ARTNO'] );

			$info['action']    = 'Updated';
			$info['variation'] = new WC_Product_Variation( $variation_posts[0]->ID );
		}

		return $info;
	}
}