<?php

namespace Buum;

use Buum\Product\Grouped;
use WP_Error;
use Buum\Product\Simple;
use Buum\Product\Variable;
use Buum\Product\Controller;

if ( ! class_exists( 'ProductImporter' ) ) :

	class ProductImporter {
		private $woo_term_by_buum_id = array();
		private $woo_term_by_map = array();

		public function reset() {
			update_option( 'buum_product_import_last', '' );
			update_option( 'buum_product_import_page', '' );
			update_option( 'buum_product_import_running', '' );
			update_option( 'buum_product_import_first_import', '' );
		}

		public function sync( $logs ) {
			$settings        = get_option( 'buum_product' );
			$lastRun         = get_option( 'buum_product_import_last' );
			$buumStock       = get_option( 'buum_stock' );
			$page            = (int) get_option( 'buum_product_import_page' );
			$firstImportDone = (int) get_option( 'buum_product_import_first_import' );
			$isRunning       = (int) get_option( 'buum_product_import_running' );
			$forceRun        = false;

			if ( $isRunning > 0 ) {
				if ( ! empty( $lastRun ) ) {
					$now  = strtotime( date( 'd.m.Y H:i:s' ) );
					$last = strtotime( $lastRun );

					// 30min
					if ( $now - $last > 1800 ) {
						$forceRun = true;
					}
				}

				if ( ! $forceRun ) {
					return;
				}
			}

			if ( ! $connect = getBuum()->getConnection() ) {
				return;
			}

			update_option( 'buum_product_import_running', 1 );
			update_option( 'buum_product_import_last', date( 'd.m.Y H:i:s' ) );

			$buumLangNo             = (int) $settings['name']['default'];
			$buumDefaultPricelistNo = (int) $settings['default_pricelist'];


			if ( ! isset( $buumStock['warehouses'] ) ) {
				$buumStock['warehouses'] = array();
			}

			$filter = array(
				'warehouses' => (array) $buumStock['warehouses'],
			);

			if ( $settings['import_artno_only'] ) {
				$query = new \WP_Query( array(
						'post_type'      => array( 'product', 'product_variation' ),
						'posts_per_page' => 100,
						'paged'          => $page ? $page : 1,
						'meta_query'     => array(
							array(
								'key'     => '_buum_id',
								'value'   => array( '' ),
								'compare' => 'NOT IN'
							),
						)
					)
				);

				if ( empty( $page ) || $page >= $query->max_num_pages ) {
					update_option( 'buum_product_import_page', 1 );
				} else {
					update_option( 'buum_product_import_page', $page + 1 );
				}

				if ( empty( $query->posts ) ) {
					return;
				}

				foreach ( $query->posts as $product_post ) {
					$buum_id      = get_post_meta( $product_post->ID, '_buum_id', true );
					$buum_product = $connect->getProductById( $buum_id, $buumDefaultPricelistNo, $buumStock['warehouses'] );

					if ( empty( $buum_product ) ) {
						continue;
					}

					$controller = new Controller( wc_get_product( $product_post->ID ), $buum_product );
					$controller->sync_artno_products();
				}

				update_option( 'buum_product_import_running', '' );
				return;
			}

			$this->sync_attributes();
			$this->sync_categories( $connect );

			if ( $firstImportDone ) {
				$filter['last_change'] = date( 'Y-m-d H:i:s', strtotime( '-3 days' ) );
			}

			$buumProducts = $connect->getProductsByLanguage( $buumLangNo, $buumDefaultPricelistNo, $filter, $page );

			if ( empty( $buumProducts ) ) {
				if ( ! $firstImportDone ) {
					update_option( 'buum_product_import_first_import', '1' );
				}
				update_option( 'buum_product_import_page', '0' );
			} else {
				update_option( 'buum_product_import_page', $page + 1 );

				foreach ( $buumProducts as $buumProduct ) {
					$type = Controller::get_product_type( $buumProduct, $connect, $buumLangNo, $buumDefaultPricelistNo, $buumStock );

					// Don`t create wp post for variation
					if ( $type === 'variation' ) {
						continue;
					}

					$result = Controller::create_product_post( $buumProduct, true );

					if ( is_null( $result ) ) {
						continue;
					}

					Controller::$categories_ids = array(
						$this->woo_term_by_buum_id,
						$this->woo_term_by_map
					);

					switch ( $type ) {
						case 'variable':
							new Variable( $result, $buumProduct, $connect, $buumLangNo, $buumDefaultPricelistNo, $buumStock, $logs );
							break;
						case 'grouped':
							new Grouped( $result, $buumProduct, $connect, $buumLangNo, $buumDefaultPricelistNo, $buumStock, $logs );
							break;
						case 'simple':
							new Simple( $result, $buumProduct, $logs );
							break;
						default:
							new Simple( $result, $buumProduct, $logs );
					}
				}
			}

			update_option( 'buum_product_import_running', '' );
		}

		public function sync_categories( $connect ) {
			$settings  = get_option( 'buum_product' );
			$woo_terms = get_terms( array(
				'taxonomy'   => 'product_cat',
				'hide_empty' => false,
			) );

			if ( $settings['categories_sync'] == 'import' ) {
				$categories = $connect->getCategories();

				foreach ( $woo_terms as $term ) {
					if ( $buum_id = get_term_meta( $term->term_id, 'buum_id', true ) ) {
						$this->woo_term_by_buum_id[ $buum_id ] = (array) $term;
					}
				}

				foreach ( $categories as $category ) {
					if ( ! isset( $this->woo_term_by_buum_id[ $category['GROUPNO'] ] ) ) {
						$term = wp_insert_term( $category['GROUPNAME'], 'product_cat' );

						if ( is_wp_error( $term ) ) {
							continue;
						}

						update_term_meta( $term['term_id'], 'buum_id', $category['GROUPNO'] );
					} else {
						$term = $this->woo_term_by_buum_id[ $category['GROUPNO'] ];

						wp_update_term( $term['term_id'], 'product_cat', array(
							'name' => $category['GROUPNAME'],
						) );
					}
				}
			} else {
				foreach ( $woo_terms as $term ) {
					$buum_ids = get_term_meta( $term->term_id, 'buum_ids', true );

					if ( ! empty( $buum_ids ) ) {
						foreach ( (array) $buum_ids as $buum_cat_id ) {
							if ( ! isset( $this->woo_term_by_map[ $buum_cat_id ] ) ) {
								$this->woo_term_by_map[ $buum_cat_id ] = array();
							}
							$this->woo_term_by_map[ $buum_cat_id ][] = (int) $term->term_id;
						}
					}
				}
			}
		}

		public function sync_attributes() {
			if ( ! $connect = getBuum()->getConnection() ) {
				return;
			}

			$buumProductSettings = get_option( 'buum_product' );
			$map                 = array(
				'color'    => __( 'Värv', BUUM_TD ),
				'producer' => __( 'Tootja', BUUM_TD ),
			);

			if ( $buumProductSettings['import_size'] ) {
				$map['size'] = __( 'Suurus', BUUM_TD );
			}

			// Register Attributes
			foreach ( $map as $key => $name ) {
				$this->process_add_attribute( array(
					'attribute_name'    => $key,
					'attribute_label'   => $name,
					'attribute_type'    => 'text',
					'attribute_orderby' => 'menu_order',
					'attribute_public'  => true
				) );
			}

			// Insert attr values
			if ( $buumProductSettings['import_color'] ) {
				$colors = $connect->getColors();
				foreach ( $colors as $color ) {
					if ( ! term_exists( $color['COLORNAME'], 'pa_color' ) ) {
						wp_insert_term( $color['COLORNAME'], 'pa_color' );
					}
				}
			}

			if ( $buumProductSettings['import_producer'] ) {
				$producers = $connect->getProducers();
				foreach ( $producers as $producer ) {
					if ( ! term_exists( $producer['PRODUCERNAME'], 'pa_producer' ) ) {
						wp_insert_term( $producer['PRODUCERNAME'], 'pa_producer' );
					}
				}
			}

			if ( $buumProductSettings['import_size'] ) {
				$sizes = $connect->getSizes();
				foreach ( $sizes as $size ) {
					if ( ! term_exists( $size['SIZENAME'], 'pa_size' ) ) {
						wp_insert_term( $size['SIZENAME'], 'pa_size' );
					}
				}
			}
		}

		public function process_add_attribute( $attribute ) {
			if ( empty( $attribute['attribute_type'] ) ) {
				$attribute['attribute_type'] = 'text';
			}
			if ( empty( $attribute['attribute_orderby'] ) ) {
				$attribute['attribute_orderby'] = 'menu_order';
			}
			if ( empty( $attribute['attribute_public'] ) ) {
				$attribute['attribute_public'] = 0;
			}

			if ( empty( $attribute['attribute_name'] ) || empty( $attribute['attribute_label'] ) ) {
				return new WP_Error( 'error', __( 'Please, provide an attribute name and slug.', 'woocommerce' ) );
			}

			if ( is_wp_error( $this->valid_attribute_name( $attribute['attribute_name'] ) ) ) {
				return $this->valid_attribute_name( $attribute['attribute_name'] );
			}

			if ( taxonomy_exists( 'pa_' . $attribute['attribute_name'] ) ) {
				return wc_attribute_taxonomy_id_by_name( $attribute['attribute_name'] );
			}

			$taxonomy_name = wc_attribute_taxonomy_name( $attribute['attribute_name'] );
			$attribute_id  = wc_create_attribute( array(
				'name'         => $attribute['attribute_label'],
				'slug'         => $attribute['attribute_name'],
				'type'         => $attribute['attribute_type'],
				'order_by'     => $attribute['attribute_orderby'],
				'has_archives' => $attribute['attribute_public'],
			) );

			register_taxonomy(
				$taxonomy_name,
				apply_filters( 'woocommerce_taxonomy_objects_' . $taxonomy_name, array( 'product' ) ),
				apply_filters( 'woocommerce_taxonomy_args_' . $taxonomy_name, array(
					'labels'       => array(
						'name' => $attribute['attribute_label'],
					),
					'hierarchical' => true,
					'show_ui'      => false,
					'query_var'    => true,
					'rewrite'      => false,
				) )
			);

			//Clear caches
			delete_transient( 'wc_attribute_taxonomies' );

			return $attribute_id;
		}

		public function valid_attribute_name( $attribute_name ) {
			if ( strlen( $attribute_name ) >= 28 ) {
				return new WP_Error( 'error', sprintf( __( 'Slug "%s" is too long (28 characters max). Shorten it, please.', 'woocommerce' ), sanitize_title( $attribute_name ) ) );
			} elseif ( wc_check_if_attribute_name_is_reserved( $attribute_name ) ) {
				return new WP_Error( 'error', sprintf( __( 'Slug "%s" is not allowed because it is a reserved term. Change it, please.', 'woocommerce' ), sanitize_title( $attribute_name ) ) );
			}

			return true;
		}
	}

endif;