<?php

namespace Buum;

class SettingsPage {
	/**
	 * Holds the values to be used in the fields callbacks
	 */
	private $options;
	private $logs;


	public function __construct( \Buum\Logs $logs ) {
		add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
		$this->logs = $logs;


		if ( isset( $_REQUEST['buum_clear_logs'] ) ) {
			$this->logs->clear();
		}
	}

	/**
	 * Add options page
	 */
	public function add_plugin_page() {
		// This page will be under "Settings"
		add_options_page(
			'Buum Logs',
			'Buum Logs',
			'manage_options',
			'buum_logs_admin',
			array( $this, 'create_admin_page' )
		);
	}

	/**
	 * Options page callback
	 */
	public function create_admin_page() {
		$this->table_styles();
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'Buum Logs', BUUM_TD ); ?></h1>
			<div class="buum-table">
				<?php new LogsTable( $this->logs ); ?>
			</div>
			<div>
				<form action="#" method="post">
					<?php
					submit_button(
						__( 'Clear Logs', BUUM_TD ),
						'primary',
						'buum_clear_logs',
						false
					);
					?>
				</form>
			</div>
		</div>
		<?php
	}

	/**
	 * Register and add settings
	 */
	public function page_init() {

	}

	private function table_styles() {
		?>
		<style type="text/css">
			.buum-table table {
				width: 100%;
				max-width: 100%;
				margin-top: 3rem;
				margin-bottom: 1rem;
				background-color: transparent;
				font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
				font-size: 1rem;
				font-weight: 400;
				line-height: 1.5;
				color: #212529;
			}

			.buum-table table td, .buum-table table th {
				padding: .75rem;
				vertical-align: top;
				border-top: 1px solid #dee2e6;
			}

			.buum-table table thead th {
				vertical-align: bottom;
				border-bottom: 2px solid #dee2e6;
				padding: .75rem;
				border-top: 1px solid #dee2e6;
			}

			.buum-table table thead {
				color: #495057;
				background-color: #e9ecef;
				border-color: #dee2e6;
			}
		</style>
		<?php
	}
}