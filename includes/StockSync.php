<?php

namespace Buum;

use Buum\Product\Controller;

class StockSync {
	public function sync( \Buum\Logs $logs ) {
		$page                    = (int) get_option( 'buum_product_stock_import_page' );
		$is_last_page            = (int) get_option( 'buum_product_stock_import_last_page' );
		$settings                = get_option( 'buum_product' );
		$buum_lang               = (int) $settings['name']['default'];
		$buum_default_price_list = (int) $settings['default_pricelist'];
		$buum_stock              = get_option( 'buum_stock' );

		if ( ! isset( $buum_stock['warehouses'] ) ) {
			$buum_stock['warehouses'] = array();
		}

		$filter = array(
			'warehouses' => (array) $buum_stock['warehouses'],
		);

		if ( ! $connect = getBuum()->getConnection() ) {
			return;
		}

		if ( $is_last_page ) {
			$filter['stock_last_change'] = date( 'Y-m-d H:i:s', strtotime( '-3 days' ) );
		}


		$buum_products = $connect->getProductsByLanguage( $buum_lang, $buum_default_price_list, $filter, $page );

		if ( empty( $buum_products ) ) {
			if ( ! $is_last_page ) {
				update_option( 'buum_product_stock_import_last_page', '1' );
			}
			update_option( 'buum_product_stock_import_page', '0' );
		} else {
			update_option( 'buum_product_stock_import_page', $page + 1 );

			foreach ( $buum_products as $buum_product ) {
				$this->process( $buum_product, $connect, $buum_lang, $buum_default_price_list, $buum_stock, $logs );
			}
		}
	}

	private function process( $data, $connect, $langNo, $defaultPriceListNo, $stock, \Buum\Logs $logs ) {
		$type       = Controller::get_product_type( $data, $connect, $langNo, $defaultPriceListNo, $stock );
		$product_id = wc_get_product_id_by_sku( $data['ARTCODE'] );

		// Skip if product do not exist yet
		if ( ! $product_id ) {
			return;
		}

		// Skip for Grouped and Variable products seems stock getting by linked products or variations
		if ( $type === 'grouped' || $type === 'variable' ) {
			return;
		}

		$product = wc_get_product( $product_id );
		$product->set_manage_stock( true );
		$product->set_stock_quantity( $data['STOCK_QTY'] );
		$product->save();
		$logs->insert( $data['ARTCODE'], $data['product_name'], 'Stock update' );
	}
}