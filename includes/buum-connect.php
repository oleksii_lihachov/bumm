<?php
if ( ! class_exists( 'Buum_Connect' ) ) :

    class Buum_Connect {

        /**
         * PDO object
         *
         * @var PDO
         */
        public $db;

        public function __construct($host, $db, $username, $password) {
            $this->connect($host, $db, $username, $password);
        }

        /**
         * Connect to database
         *
         * @param $host
         * @param $db
         * @param $username
         * @param $password
         */
		protected function connect( $host, $db, $username, $password ) {
			$this->db = new PDO( "mysql:host=$host;dbname=$db", $username, $password, array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
			) );
			$this->db->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
			$this->addWooCommerceTrigger( $host, $username, $password, $db );
		}

		private function addWooCommerceTrigger( $host, $username, $password, $db ) {
			$K    = 'strval';

			// ITEM delete trigger
			$this->db->exec( "DROP TRIGGER IF EXISTS TR_ITEM_AFTER_DELETE" );
			$sql  = "
			CREATE TRIGGER TR_ITEM_AFTER_DELETE
			AFTER DELETE ON `{$db}`.`ITEM` FOR EACH ROW
			BEGIN
				UPDATE `{$K(DB_NAME)}`.`wp_postmeta`
			 	SET
			 		meta_value = 'deleted'
			 	WHERE
			 		(meta_key = '_buum_id' and meta_value = OLD.ARTNO) OR (meta_key = '_buum_variation_id' and meta_value = OLD.ARTCODE);
			 	INSERT INTO `{$K(DB_NAME)}`.`wp_buum_logs` (`buum_sku`, `product`, `action`) VALUES (OLD.ARTNO, OLD.ARTNAME, 'Deleted');	
			 END
			";
			$stmt = $this->db->prepare( $sql );
			$stmt->execute();

			// BARCODE delete trigger
			$this->db->exec( "DROP TRIGGER IF EXISTS TR_BARCODE_AFTER_DELETE" );
			$sql = "
			CREATE TRIGGER TR_BARCODE_AFTER_DELETE
			AFTER DELETE ON `{$db}`.`BARCODE` FOR EACH ROW
			BEGIN
				UPDATE `{$K(DB_NAME)}`.`wp_postmeta`
			 	SET
			 		meta_value = 'deleted'
			 	WHERE
			 		meta_key = '_buum_variation_id' and meta_value = OLD.BARCODE;
			 	INSERT INTO `{$K(DB_NAME)}`.`wp_buum_logs` (`buum_sku`, `product`, `action`) VALUES (OLD.ARTNO, OLD.BARCODE, 'Deleted');	
			 END
			";
			$stmt = $this->db->prepare( $sql );
			$stmt->execute();

			// BARCODE reset SIZENO trigger
			$this->db->exec( "DROP TRIGGER IF EXISTS TR_BARCODE_AFTER_SIZENO_UPDATE" );
			$sql  = "
			CREATE TRIGGER TR_BARCODE_AFTER_SIZENO_UPDATE
			AFTER UPDATE ON `{$db}`.`BARCODE` FOR EACH ROW 
			BEGIN
				IF NEW.SIZENO IS NULL THEN
					UPDATE `{$K(DB_NAME)}`.`wp_postmeta`
					SET
			 			meta_value = 'deleted'
			 		WHERE
			 			meta_key = '_buum_variation_id' and meta_value = OLD.BARCODE;
			 		INSERT INTO `{$K(DB_NAME)}`.`wp_buum_logs` (`buum_sku`, `product`, `action`) VALUES (OLD.ARTNO, OLD.BARCODE, 'Deleted');
			 	END IF;
			END
			";
			$stmt = $this->db->prepare( $sql );
			$stmt->execute();

			$wp_db = new PDO( "mysql:host=$host;dbname={$K(DB_NAME)}", $username, $password, array(
				PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
			) );
			$wp_db->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC );
			$wp_db->exec( "DROP TRIGGER IF EXISTS BUUM_META_AFTER_UPDATE" );
			$sql  = "
			CREATE TRIGGER BUUM_META_AFTER_UPDATE
			AFTER UPDATE ON `{$K(DB_NAME)}`.`wp_postmeta` FOR EACH ROW 
			BEGIN
				IF NEW.meta_value = 'deleted' THEN
					UPDATE `{$K(DB_NAME)}`.`wp_posts`
					SET 
						post_status = 'trash'
					WHERE
						ID = OLD.post_id;
				 END IF;
			END
			";
			$stmt = $wp_db->prepare( $sql );
			$stmt->execute();
		}

        public function insert($table, $data = array()) {

            $columns = array();
            $values = array();

            foreach ($data as $k => $i) {
                if (strlen($i) > 0) {
                    $columns[] = $k;
                    $values[] = $i;
                }
            }

            $columnPlaceholders = array();
            foreach ($columns as $col) {
                $columnPlaceholders[] = '?';
            }
            $columnPlaceholders = implode(',', $columnPlaceholders);

            $columns = implode(',', $columns);

            $sql = "INSERT INTO `$table` ($columns) VALUES ($columnPlaceholders)";

            $stmt = $this->db->prepare($sql);
            $stmt->execute($values);
            return $this->db->lastInsertId();

        }

        public function getLanguages() {
            $stmt = $this->db->prepare("SELECT * FROM LANGUAGE");
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function getCustomerByEmail($email) {
            if (empty($email)) {
                return array();
            }
            $stmt = $this->db->prepare("
                SELECT
                    c.CUSTNO,
                    c.CUSTCODE,
                    c.CUSTNAME,
                    c.DISCOUNT,
                    c.EMAIL
                FROM CUMAIN AS c
                WHERE c.EMAIL LIKE ?
            ");

            $stmt->execute(array($email));

            return $stmt->fetch();
        }

        public function getCustomerByCode($code) {
            if (empty($code)) {
                return array();
            }
            $stmt = $this->db->prepare("
                SELECT
                    c.CUSTNO,
                    c.CUSTCODE,
                    c.CUSTNAME,
                    c.DISCOUNT,
                    c.EMAIL
                FROM CUMAIN AS c
                WHERE c.CUSTCODE LIKE ?
            ");

            $stmt->execute(array($code));

            return $stmt->fetch();
        }

        public function getItemByCode($code) {
            if (empty($code)) {
                return array();
            }
            $stmt = $this->db->prepare("
                SELECT * FROM ITEM WHERE ARTCODE LIKE ?
            ");

            $stmt->execute(array($code));

            return $stmt->fetch();
        }

        public function getCustomers($filter = array(), $page, $perPage = 90) {
            $currentPage = $page * $perPage;

            $where = array('1 = 1');
            $params = array();

            if (!empty(array_filter(array_map('intval', (array) $filter['group_id_in'])))) {
                $groupNumbers = array_filter(array_map('intval', (array) $filter['group_id_in']));
                $where[] = 'c.CUSTGROUPNO IN (' . implode(',', $groupNumbers) . ')';
            }

            if (!empty($filter['last_change'])) {
                $where[] = 'c.LASTCHANGE >= ?';
                $params[] = $filter['last_change'];
            }

            $stmt = $this->db->prepare("
                SELECT
                    c.CUSTNO,
                    c.CUSTCODE,
                    c.CUSTNAME,
                    c.DISCOUNT,
                    c.PRICEGROUPNO,
                    c.EMAIL
                FROM CUMAIN AS c
                WHERE " . implode(' AND ', $where) . "
                ORDER BY c.LASTCHANGE ASC
                LIMIT $currentPage, $perPage
            ");

            $stmt->execute($params);

            return $stmt->fetchAll();
        }

        public function getCustomerGroups() {
            $stmt = $this->db->prepare("SELECT * FROM CUSTGROUP ORDER BY custgroupname ASC");
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function getProductsByLanguage($langNo, $pricelistNo, $filter = array(), $page = 0, $perPage = 90) {
            $currentPage = $page * $perPage;

            $where = array();
            $where[] = 'i.ARTNO IS NOT NULL';
            //$where[] = "i.EXPORTABLE = 'T'";

            $params = array($langNo, $pricelistNo);

            $stocks = array(0);

            if (!empty($filter['warehouses'])) {
                $warehouses = array_merge($stocks, array_map('intval', (array) $filter['warehouses']));
            }

            $warehouses = implode(',', $warehouses);

            if (!empty($filter['last_change'])) {
                $where[] = "GREATEST(COALESCE((SELECT MAX(LASTCHANGE) FROM STOCK WHERE ARTNO = i.ARTNO),'2000-01-01'), COALESCE(p.LASTCHANGE,'2000-01-01'),COALESCE(i.LASTCHANGE,'2000-01-01')) >= ?";
                $params[] = $filter['last_change'];
            }

			if ( ! empty( $filter['stock_last_change'] ) ) {
				$where[]  = "COALESCE((SELECT MAX(LASTCHANGE) FROM STOCK WHERE ARTNO = i.ARTNO), '2000-01-01') >= ?";
				$params[] = $filter['stock_last_change'];
			}

			if ( ! empty( $filter['variations'] ) ) {
				$where[] = 'i.OWNERNO LIKE ' . $filter['variations'];
			}

            $stmt = $this->db->prepare("
                SELECT
                    i.ARTNO,
                    i.EXPORTABLE,
             		i.ARTTYPE,
             		i.OWNERNO,
                    COALESCE(an.NAME, i.ARTNAME) AS product_name,
                    i.ARTNAME,
                    i.ARTCODE,
                    i.WEIGHT,
                    i.WIDTH,
                    i.LEN,
                    i.HEIGHT,
                    i.GROUPNO,
                    i.INFO1,
                    i.INFO2,
                    i.INFO3,
                    i.INFO4,
                    i.INFO5,
                    i.INFO6,
                    p.PRICE,
                    (
                        SELECT GROUP_CONCAT(ARTNO, ',') 
                        FROM BOUNDART 
                        WHERE MASTERARTNO = i.ARTNO
                    ) AS related_articles,
                    COLOR.COLORNAME,
                    PRODUCER.PRODUCERNAME,
                    ARTSIZE.SIZENAME,
                    ARTNAMES.INFO AS product_desc,
                    (
                        SELECT SUM(VOLUME) FROM STOCK
                        WHERE ARTNO = i.ARTNO AND STOCKNO IN ($warehouses)
                    ) AS STOCK_QTY,
                    (
                        SELECT
                            PRICE
                        FROM PRICES
                        WHERE ARTNO = i.ARTNO AND PASSIVE = 'F'
                        ORDER BY LASTCHANGE DESC
                        LIMIT 1
                    ) AS LAST_PRICE
                FROM ITEM AS i
                LEFT JOIN ARTNAMES AS an ON an.ARTNO = i.ARTNO AND an.LANGNO = ?
                LEFT JOIN PRICES AS p ON p.ARTNO = i.ARTNO AND p.PRICELISTNO = ? AND p.PASSIVE = 'F'
                LEFT JOIN COLOR ON COLOR.COLORNO = i.COLORNO
                LEFT JOIN PRODUCER ON PRODUCER.PRODUCERNO = i.PRODUCERNO
                LEFT JOIN ARTSIZE ON ARTSIZE.SIZENO = i.SIZENO
                LEFT JOIN ARTNAMES ON ARTNAMES.ARTNO = i.ARTNO
                WHERE " . implode(' AND ', $where) . "
                ORDER BY i.LASTCHANGE ASC
                LIMIT $currentPage, $perPage
            ");



            $stmt->execute($params);

            return $stmt->fetchAll();
        }

		public function getProductById( $id, $pricelistNo, $warehouses ) {
			$stocks     = array( 0 );
			$warehouses = array_merge( $stocks, array_map( 'intval', $warehouses ) );
			$warehouses = implode( ', ', $warehouses );

			$stmt = $this->db->prepare( "
				SELECT 
					p.PRICE,
					 (
                        SELECT SUM(VOLUME) FROM STOCK
                        WHERE ARTNO = i.ARTNO AND STOCKNO IN ($warehouses)
                    ) AS STOCK_QTY
				FROM ITEM AS i
				LEFT JOIN PRICES AS p ON p.ARTNO = i.ARTNO AND p.PRICELISTNO = ? AND p.PASSIVE = 'F'
				WHERE i.ARTNO = ?
			" );

			$stmt->execute(array(
				$pricelistNo,
				$id
			));

			return $stmt->fetch();
		}

		public function getBarcodeProduct( $id, $warehouses ) {
			$stocks     = array( 0 );
			$warehouses = array_merge( $stocks, array_map( 'intval', $warehouses ) );
			$warehouses = implode( ', ', $warehouses );

			$stmt = $this->db->prepare( "
			   SELECT
			   		i.BARCODE,
			   		i.SIZENO,
			   		i.ISDEFAULT,
			   		(
			   		SELECT SUM(VOLUME) FROM STOCK	
					WHERE ARTNO = i.ARTNO AND SIZENO = i.SIZENO AND STOCKNO IN ($warehouses)
			   		) AS STOCK_QTY   		
				FROM BARCODE as i
				WHERE i.ARTNO = :id AND i.SIZENO IS NOT NULL
			" );

			$stmt->execute( array(
				'id' => $id,
			) );
			return $stmt->fetchAll();
		}

        public function getPricelists() {
            $stmt = $this->db->prepare("SELECT * FROM PRICELIST");
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function getWarehouses() {
            $stmt = $this->db->prepare("SELECT * FROM STOCKCOD");
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function getCategories() {
            $stmt = $this->db->prepare("
                SELECT
                    ag.METAGROUPNO,
                    ag.GROUPNO,
                    ag.GROUPCODE,
                    ag.GROUPNAME,
                    (
                        CASE WHEN mg.PARENTNO IS NULL THEN NULL WHEN EXISTS (
                            SELECT 1 FROM METAGRUP WHERE METAGROUPNO = mg.PARENTNO
                            
                        ) THEN mg.PARENTNO ELSE NULL END
                    ) AS PARENTNO
                FROM ARTGRP AS ag
                LEFT JOIN METAGRUP AS mg ON mg.METAGROUPNO = ag.METAGROUPNO
                WHERE 
                    ag.EXPORTABLE = 'T'
            ");
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function getProductPriceByCustomerId($artNo, $customerId) {
            if (strlen($artNo) < 1 || strlen($customerId) < 1) {
                return false;
            }

            $stmt = $this->db->prepare("
                SELECT
                    PRICE
                FROM PRICES AS p
                JOIN PRICELIST AS pl ON pl.PRICELISTNO = p.PRICELISTNO
                WHERE 
                    p.ARTNO = ? AND 
                    p.PASSIVE = 'F' AND 
                    pl.CUSTNO = ? AND 
                    pl.PRICEBLOCKED = 'F'
                ORDER BY p.LASTCHANGE DESC
            ");
            $stmt->execute(array($artNo, (int) $customerId));
            $row = $stmt->fetch();
            return $row['PRICE'];

        }

        public function getProductPriceByPricelist($artNo, $pricelistId) {
            if (strlen($artNo) < 1 || strlen($pricelistId) < 1) {
                return false;
            }
            $stmt = $this->db->prepare("
                SELECT
                    PRICE
                FROM prices WHERE ARTNO = ? AND PASSIVE = 'F' AND PRICELISTNO = ?
            ");
            $stmt->execute(array($artNo, $pricelistId));
            $row = $stmt->fetch();
            return $row['PRICE'];
        }

        public function getInvoiceTypes() {
            return array(
                100 => __('Arve', BUUM_TD),
                103 => __('Tellimus', BUUM_TD),
                109 => __('Broneering', BUUM_TD),
            );
        }

        public function getColors() {
            $stmt = $this->db->prepare("SELECT * FROM COLOR");
            $stmt->execute();
            return $stmt->fetchAll();
        }

        public function getProducers() {
            $stmt = $this->db->prepare("SELECT * FROM PRODUCER");
            $stmt->execute();
            return $stmt->fetchAll();
        }

		public function getSizes() {
			$stmt = $this->db->prepare("SELECT * FROM ARTSIZE");
			$stmt->execute();
			return $stmt->fetchAll();
		}

		public function getSizeById( $id ) {
			$stmt = $this->db->prepare( "SELECT SIZENAME FROM ARTSIZE WHERE SIZENO = ?" );
			$stmt->execute( array( $id ) );
			return $stmt->fetch();
		}
    }



endif;