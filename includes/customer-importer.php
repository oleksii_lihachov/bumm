<?php

if ( ! class_exists( 'Buum_Customer_Importer' ) ) :

class Buum_Customer_Importer {

    public function __construct() {

    }

    public function reset() {
        update_option('buum_customer_import_last', '');
        update_option('buum_customer_import_page', '');
        update_option('buum_customer_import_running', '');
        update_option('buum_customer_import_first_import', '');
    }

    public function sync() {
        $lastRun = get_option('buum_customer_import_last');
        $page = (int) get_option('buum_customer_import_page');
        $firstImportDone = (int) get_option('buum_customer_import_first_import');
        $isRunning = (int) get_option('buum_customer_import_running');

        if ($isRunning > 0) {


            if (!empty($lastRun)) {
                $now = strtotime(date('d.m.Y H:i:s'));
                $last = strtotime($lastRun);

                // 30min
                if ($now - $last > 1800) {
                    $forceRun = true;
                }
            }

            if (!$forceRun) {
                return;
            }

        }

        if (!$connect = getBuum()->getConnection()) {
            return;
        }

        update_option('buum_customer_import_running', 1);
        update_option('buum_customer_import_last', date('d.m.Y H:i:s'));


        $buumCustomerSettings = get_option('buum_customer');
        if (!isset($buumCustomerSettings['group_ids'])) {
            $buumCustomerSettings['group_ids'] = array();
        }
        $buumCustomerSettings['group_ids'] = (array) $buumCustomerSettings['group_ids'];

        $filter = array();

        if (!empty($buumCustomerSettings['group_ids'])) {
            $filter['group_id_in'] = $buumCustomerSettings['group_ids'];
        }

        if ($firstImportDone) {
            $filter['last_change'] = date('Y-m-d H:i:s', strtotime('-3 days'));
        }

        $buumClients = $connect->getCustomers($filter, $page);

        if (empty($buumClients)) {
            if (!$firstImportDone) {
                update_option('buum_customer_import_first_import', '1');
            }
            update_option('buum_customer_import_page', '0');
        }
        else {
            update_option('buum_customer_import_page', $page + 1);


            foreach ($buumClients as $buumClient) {

                $code = $buumClient['CUSTCODE'];

                $users = get_users(array(
                    'meta_key' => 'buum_code',
                    'meta_value' => $code,
                ));

                $userId = null;

                if (empty($users)) {

                    if (!$userId = username_exists($code)) {
                        $random_password = wp_generate_password( $length = 12, false );
                        $userId = wp_create_user( $code, $random_password, $buumClient['EMAIL'] );

                        if ($userId instanceof WP_Error) {
                            continue;
                        }

                        $wp_user_object = new WP_User($userId);
                        $wp_user_object->set_role('customer');

                        update_user_meta( $userId, "billing_first_name", $buumClient['CUSTNAME'] );
                    }

                    update_user_meta( $userId, "buum_id", $buumClient['CUSTNO'] );
                    update_user_meta( $userId, "buum_code", $code );
                    update_user_meta( $userId, "buum_pricelist_no", $buumClient['PRICEGROUPNO'] );

                }
                else {
                    $user = array_shift($users);
                    $userId = $user->ID;

                }

                if ($userId) {
                    update_user_meta( $userId, "buum_id", $buumClient['CUSTNO'] );
                    update_user_meta($userId, '_discount', (int) $buumClient['DISCOUNT']);
                    update_user_meta($userId, "buum_pricelist_no", (int) $buumClient['PRICEGROUPNO'] );
                }



            }

        }

        update_option('buum_customer_import_running', '');

    }


}

endif;