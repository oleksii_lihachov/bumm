<?php

if ( ! class_exists( 'Buum_Invoice_Export' ) ) :

class Buum_Invoice_Export {

    protected $invoice_settings = array();
    protected $customer_settings = array();

    public function __construct() {
        $this->invoice_settings = get_option('buum_invoice');
        $this->customer_settings = get_option('buum_customer');
        if (empty($this->invoice_settings['default_stock'])) {
            $this->invoice_settings['default_stock'] = 1;
        }
    }

    public function sync() {
        $invoices = get_posts(array(
            'post_type' => 'shop_order',
            'post_status' => array('wc-processing', 'wc-completed'),
            'posts_per_page' => -1,
            'order' => 'DESC',
            'orderby' => 'ID',
            'meta_query' => array(
                array(
                    'key' => 'buum_invoice_id',
                    'compare' => 'NOT EXISTS'
                ),
            )
        ));

        foreach ($invoices as $invoice) {
            $this->sendInvoice($invoice->ID);
        }
    }

    public function sendInvoice($order_id) {

        if (get_post_meta($order_id, 'buum_invoice_id', true)) {
            return;
        }

        if (!$order = wc_get_order($order_id)) {
            return;
        }

        if (!$connect = getBuum()->getConnection()) {
            return;
        }

        $userId = $order->get_user_id();

        $custCode = null;

        if ($userId > 0) {
            $custCode = get_user_meta( $userId, "buum_code", true );
        }

        $estimatedCustomerCode = $this->customer_settings['prefix'] . $order_id;

        if (empty($custCode)) {
            if ($buumCustomer = $connect->getCustomerByEmail($order->get_billing_email())) {
                $custCode = $buumCustomer['CUSTCODE'];
            }
            elseif ($buumCustomer = $connect->getCustomerByCode($estimatedCustomerCode)) {
                $custCode = $buumCustomer['CUSTCODE'];
            }
        }

        if (empty($custCode)) {
            $firm = 'F';
            if ($order->get_billing_company()) {
                $customerName = $order->get_billing_company();
                $firm = 'T';
            }
            else {
                $customerName = implode(' ', array_filter(array($order->get_billing_first_name(), $order->get_billing_last_name())));
            }

            $customerData = array(
                'CUSTCODE' => $estimatedCustomerCode,
                'CUSTNAME' => $customerName,
                'EMAIL' => $order->get_billing_email(),
                'PHONE' => $order->get_billing_phone(),
                'TYPENO' => 1,
                'CUSTGROUPNO' => $this->customer_settings['default_group'],
                'FIRM' => $firm,
                'ADDRESS' => $order->get_billing_address_1(),
                'ADDRESS2' => $order->get_billing_address_2(),
                'ADDRESS3' => $order->get_billing_city(),
                'ADDRESS4' => $order->get_billing_country(),
                'ADDRESS5' => $order->get_billing_postcode(),
            );
            if ($id = $connect->insert('CUMAIN', $customerData)) {
                $custCode = $estimatedCustomerCode;
                $connect->insert('TRANS_LOG', array(
                    'TABLE_NAME' => 'CUMAIN',
                    'ID' => $id,
                    'EVENT_CODE' => 'A',
                ));
            }
        }

        if (empty($custCode)) {
            return false;
        }

        $data = array(
            'INVOICE' => $this->invoice_settings['prefix'] . $order->get_order_number(),
            'STOCKNO' => $this->invoice_settings['default_stock'],
            'CUSTCODE' => $custCode,
            'TYPE' => $this->invoice_settings['type'],
            'INVOICEDATE' => date('Y-m-d', strtotime($order->get_date_created())),
            'AMOUNT' => $order->get_total(),
            'VATAMOUNT' => $order->get_total_tax(),
            'COMMENT' => $order->get_order_number(),
        );

        if ($invoiceId = $connect->insert('INVOICE', $data)) {

            $invoiceLines = array();

            update_post_meta($order_id, 'buum_invoice_id', $invoiceId);

			foreach ( $order->get_items( 'line_item' ) as $item_id => $item ) {
				$invoice_data = array();

				if ( $item['variation_id'] ) {
					$product = wc_get_product( $item['variation_id'] );

					if ( $sizeno = get_post_meta( $product->get_id(), '_buum_variation_sizeno', true ) ) {
						$invoice_data['SIZENO'] = $sizeno;
					}
				} else {
					$product = wc_get_product( $item['product_id'] );
				}

				if ( ! $buumProductId = get_post_meta( $product->get_id(), '_buum_id', true ) ) {
					continue;
				}

				$invoice_data['VATAMOUNT'] = $order->get_line_tax( $item );
				$invoice_data['AMOUNT']    = $order->get_line_total( $item, true, true );
				$invoice_data['QTY']       = $item->get_quantity();
				$invoice_data['ARTNO']     = $buumProductId;
				$invoice_data['INVNO']     = $invoiceId;

				$invoiceLines[] = $invoice_data;
			}

            $line_items_shipping = $order->get_items( 'shipping' );

            foreach ($line_items_shipping as $item_id => $item) {

                $full_instance = implode(':', array_filter(array($item->get_method_id(), $item->get_instance_id())));
                if (empty($this->invoice_settings['shipping_article'][$full_instance])) {
                    continue;
                }


                $articleCode = $this->invoice_settings['shipping_article'][$full_instance];

                if ($article = $connect->getItemByCode($articleCode)) {
                    $invoiceLines[] = array(
                        'VATAMOUNT' => $order->get_line_tax($item),
                        'AMOUNT' => $order->get_item_total($item, true, true),
                        'QTY' => $item->get_quantity(),
                        'ARTNO' => $article['ARTNO'],
                        'INVNO' => $invoiceId,
                    );

                }

            }
            $lineNo = 1;
            foreach ($invoiceLines as $line) {
                $line['LINENO'] = $lineNo;
                $connect->insert('OUTITEMS', $line);
                $lineNo ++;
            }


        }

        $connect->insert('TRANS_LOG', array(
            'TABLE_NAME' => 'INVOICE',
            'ID' => $invoiceId,
            'EVENT_CODE' => 'A',
        ));


    }


}

endif;