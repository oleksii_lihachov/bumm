<?php

if ( ! class_exists( 'Buum_Price_Calculator' ) ) :

    class Buum_Price_Calculator
    {

        public function __construct()
        {
            if (!is_admin()) {
                add_filter('woocommerce_product_get_regular_price', array($this, 'custom_dynamic_regular_price'), 10, 2);
                add_filter('woocommerce_product_variation_get_regular_price', array($this, 'custom_dynamic_regular_price'), 10, 2);

                add_filter('woocommerce_product_get_sale_price', array($this, 'custom_dynamic_sale_price'), 10, 2);
                add_filter('woocommerce_product_variation_get_sale_price', array($this, 'custom_dynamic_sale_price'), 10, 2);

                add_filter('woocommerce_get_price_html', array($this, 'woocommerce_get_price_html'), 9999, 2);
                add_action('woocommerce_before_calculate_totals', array($this, 'woocommerce_before_calculate_totals'), 9999);
            }
        }

        public function custom_dynamic_regular_price( $regular_price, $product ) {
            if( empty($regular_price) || $regular_price == 0 )
                return $product->get_price();
            else
                return $regular_price;
        }

        public function custom_dynamic_sale_price( $sale_price, $product ) {
            if (!$userId = get_current_user_id()) {
                return $sale_price;
            }
            if ($buumCustomerId = get_user_meta($userId, 'buum_id', true)) {

                if ($connect = getBuum()->getConnection()) {

                    if ($artNo = get_post_meta($product->get_id(), '_buum_id', true)) {

                        if ($customPrice = $connect->getProductPriceByCustomerId($artNo, $buumCustomerId)) {

                            return number_format($customPrice, 2, '. ', '');
                        }

                    }

                }

            }

            if ($pricelistNo = get_user_meta($userId, 'buum_pricelist_no', true)) {

                if ($connect = getBuum()->getConnection()) {

                    if ($artNo = get_post_meta($product->get_id(), '_buum_id', true)) {

                        if ($customPrice = $connect->getProductPriceByPricelist($artNo, $pricelistNo)) {
                            return number_format($customPrice, 2, '. ', '');
                        }

                    }

                }

            }

            if (!$discount = get_user_meta($userId, '_discount', true)) {
                return $sale_price;
            }

            $discountSum = $product->get_price() * $discount / 100;

            return number_format($product->get_price() - $discountSum, 2, '.', '');
        }


        public function woocommerce_get_price_html( $price_html, $product ) {
            if( $product->is_type('variable') ) return $price_html;

            if (!$product->get_sale_price()) {
                return $price_html;
            }

            $price_html = wc_format_sale_price( wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price() ) ), wc_get_price_to_display(  $product, array( 'price' => $product->get_sale_price() ) ) ) . $product->get_price_suffix();

            return $price_html;

        }



        public function woocommerce_before_calculate_totals( $cart ) {

            if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

            if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 ) return;

            // IF CUSTOMER NOT LOGGED IN, DONT APPLY DISCOUNT
            if ($userId = get_current_user_id()) {

                if ($connect = getBuum()->getConnection()) {

                    if ($buumCustomerId = get_user_meta($userId, 'buum_id', true)) {

                        $pricelistNo = get_user_meta($userId, 'buum_pricelist_no', true);
                        $discount = get_user_meta($userId, '_discount', true);

                        if ($connect = getBuum()->getConnection()) {

                            foreach ($cart->get_cart() as $cart_item_key => $cart_item) {
                                $product = $cart_item['data'];

                                if ($artNo = get_post_meta($product->get_id(), '_buum_id', true)) {

                                    if ($customPrice = $connect->getProductPriceByCustomerId($artNo, $buumCustomerId)) {
                                        $cart_item['data']->set_price($customPrice);
                                    }
                                    elseif ($customPrice = $connect->getProductPriceByPricelist($artNo, $pricelistNo)) {
                                        $cart_item['data']->set_price($customPrice);
                                    }
                                    elseif ($discount) {
                                        $discountSum = $product->get_price() * $discount / 100;
                                        $cart_item['data']->set_price($product->get_price() - $discountSum);

                                    }

                                }

                            }

                        }

                    }


                }

            }

        }


    }

endif;