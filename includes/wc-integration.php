<?php

use Buum\Logs;
use Buum\SettingsPage;

if ( ! class_exists( 'WC_Buum_Integration' ) ) :

    class WC_Buum_Integration extends WC_Integration {

        const CRON_FIVE_MINUTES = 'five_minutes';

        protected $cron_schedule_product_import = 'buum_product_import';
        protected $cron_product_import_action = 'buum_product_import_action';

        protected $cron_schedule_customer_import = 'buum_customer_import';
        protected $cron_customer_import_action = 'buum_customer_import_action';

        protected $cron_schedule_invoice_export = 'buum_invoice_export';
        protected $cron_invoice_export_action = 'buum_invoice_export_action';

		protected $cron_schedule_logs_clear = 'buum_logs_clear';
		protected $cron_logs_clear_action = 'buum_logs_clear_action';

		protected $cron_stock_sync = 'buum_stock_sync';
		protected $cron_stock_sync_action = 'buum_stock_sync_action';

        private $logs;
        private $time_schedules = array(
			'five_minutes' => array(
				'interval' => 60 * 5,
				'display'  => '5min',
			),
			'hourly'       => array(
				'interval' => HOUR_IN_SECONDS,
				'display'  => '1h',
			),
			'three_hours'  => array(
				'interval' => HOUR_IN_SECONDS * 3,
				'display'  => '3h',
			),
			'six_hours'    => array(
				'interval' => HOUR_IN_SECONDS * 6,
				'display'  => '6h',
			),
			'twice_daily'  => array(
				'interval' => HOUR_IN_SECONDS * 12,
				'display'  => 'Twice Daily',
			),
			'daily'        => array(
				'interval' => DAY_IN_SECONDS,
				'display'  => 'Daily',
			),
			'two_day'      => array(
				'interval' => DAY_IN_SECONDS * 2,
				'display'  => '2d',
			),
			'three_day'    => array(
				'interval' => DAY_IN_SECONDS * 3,
				'display'  => '3d',
			),
		);

        /**
         * Init and hook in the integration.
         */
        public function __construct() {
            $this->id                 = 'buum-integration';
            $this->method_title       = 'Buum integration';
            $this->method_description = __('Buumi ja Woocommerce andmevahetuse integratsioon', BUUM_TD);

            // Load the settings
            $this->init_form_fields();
            $this->init_settings();

            // Set variables
            $this->load_options();

            add_filter('cron_schedules', array($this, 'cron_schedules'));

            // Actions
            add_action( 'init', array( $this, 'init' ) );
            add_action( 'woocommerce_update_options_integration_' . $this->id, array( $this, 'process_admin_options' ) );
            add_action( 'woocommerce_update_options_integration_' . $this->id, array( $this, 'process_admin_custom_options' ) );



            // Cron hook executer
            add_action( 'woocommerce_settings_saved', array( $this, 'schedule_cron' ) );
            add_action( $this->cron_product_import_action, array( $this, 'cron_product_import' ) );
            add_action( $this->cron_customer_import_action, array( $this, 'cron_customer_import' ) );
            add_action( $this->cron_invoice_export_action, array( $this, 'cron_invoice_export' ) );
			add_action( $this->cron_logs_clear_action, array( $this, 'cron_logs_clear' ) );
			add_action( $this->cron_stock_sync_action, array( $this, 'cron_stock_sync' ) );

            add_action('product_cat_add_form_fields', array($this, 'add_category_field'));
            add_action('product_cat_edit_form_fields', array($this, 'add_category_field'));

            add_action('edited_product_cat', array($this, 'save_taxonomy_custom_meta'), 10, 1);
            add_action('create_product_cat', array($this, 'save_taxonomy_custom_meta'), 10, 1);

			$logs       = new Logs();
			$this->logs = $logs;
			new SettingsPage( $logs );

			add_action( 'woocommerce_product_options_sku', array( $this, 'create_buum_id_field' ), 10, 0 );
			add_action( 'woocommerce_process_product_meta', array( $this, 'update_buum_id_field' ), 10, 1 );
			add_action( 'woocommerce_variation_options', array( $this, 'create_variation_buum_id_field' ), 10, 3 );
			add_action( 'woocommerce_save_product_variation', array( $this, 'update_variation_buum_id_field' ), 10, 2 );
        }


		public function save_taxonomy_custom_meta( $term_id ) {
			if ( isset( $_POST['buum_ids'] ) ) {
				update_term_meta( $term_id, 'buum_ids', (array) $_POST['buum_ids'] );
			}
		}

        public function add_category_field($term) {
            if (!$connect = getBuum()->getConnection()) {
                return;
            }
            $term_id = $term->term_id;
            $buumIds = (array) get_term_meta($term_id, 'buum_ids', true);

            $categories = $connect->getCategories();

            ?>
            <tr class="form-field">
                <th scope="row" valign="top"><label><?php _e('Buum kategooria', BUUM_TD); ?></label></th>
                <td>
                    <select name="buum_ids[]" multiple>
                        <?php foreach ($categories as $category): ?>
                            <option value="<?php echo $category['GROUPNO']; ?>"<?php if (in_array($category['GROUPNO'], $buumIds)): ?> selected<?php endif; ?>><?php echo esc_html($category['GROUPNAME']); ?></option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>
            <?php
        }

        public function cron_schedules($schedules) {
        	foreach ( $this->time_schedules as $time => $info) {
				if ( ! isset( $schedules[$time] ) ) {
					$schedules[ $time ] = $info;
				}
			}

            return $schedules;
        }

        public function admin_options() {
            parent::admin_options();
            $database = get_option('buum_database');
            $product = get_option('buum_product');
            $buum_stock = get_option('buum_stock');
            $buum_customer = get_option('buum_customer');
            $buum_invoice = get_option('buum_invoice');
            $logs_time = get_option('buum_logs_time');

            if (!isset($buum_stock['warehouses'])) {
                $buum_stock['warehouses'] = array();
            }
            $buum_stock['warehouses'] = (array) $buum_stock['warehouses'];

            if (!isset($buum_customer['group_ids'])) {
                $buum_customer['group_ids'] = array();
            }
            $buum_customer['group_ids'] = (array) $buum_customer['group_ids'];
            if (!isset($buum_customer['prefix'])) {
                $buum_customer['prefix'] = 'WP';
            }
            ?>

            <div class="buum-setting-block">
                <h2><?php _e('Buum andmebaasi seaded', BUUM_TD); ?></h2>
                <table class="form-table">
                    <tr>
                        <th><?php _e('Host', BUUM_TD); ?></th>
                        <td><input type="text" name="buum_database[host]" value="<?php if (isset($database['host'])) { echo esc_attr($database['host']); } ?>" /></td>
                    </tr>
                    <tr>
                        <th><?php _e('Andmebaas', BUUM_TD); ?></th>
                        <td><input type="text" name="buum_database[db]" value="<?php if (isset($database['db'])) { echo esc_attr($database['db']); } ?>" /></td>
                    </tr>
                    <tr>
                        <th><?php _e('Kasutaja', BUUM_TD); ?></th>
                        <td><input type="text" name="buum_database[user]" value="<?php if (isset($database['user'])) { echo esc_attr($database['user']); } ?>" /></td>
                    </tr>
                    <tr>
                        <th><?php _e('Parool', BUUM_TD); ?></th>
                        <td><input type="password" name="buum_database[pass]" value="****" /></td>
                    </tr>
                </table>
            </div>

            <?php if ($connect = getBuum()->getConnection()): ?>

            <div class="buum-setting-block">
                <h2><?php _e('Toodete seaded', BUUM_TD); ?></h2>
                <table class="form-table">

                    <tr>
                        <th><?php _e('Kategooriate import', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_product[categories_sync]">
                                <option value=""><?php _e('Määra woocommerce kategooriate seos Buumi kategooriatega', BUUM_TD); ?></option>
                                <option value="import"<?php if ($product['categories_sync'] == 'import'): ?> selected<?php endif; ?> ><?php _e('Impordi kategooriad Buumist', BUUM_TD); ?></option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><?php _e('Vaikimisi hinnakiri', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_product[default_pricelist]">
                                <option value="-10">Vaikimisi</option>
                                <?php foreach ($connect->getPricelists() as $pricelist): ?>
                                <option value="<?php echo esc_html($pricelist['PRICELISTNO']); ?>"<?php if ($pricelist['PRICELISTNO'] == $product['default_pricelist']): ?> selected<?php endif; ?>><?php echo esc_html($pricelist['PRICELISTNAME']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th><?php _e('Impordi tootja', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_product[import_producer]">
                                <option value=""><?php _e('Ei', BUUM_TD); ?></option>
                                <option value="1"<?php if ($product['import_producer']): ?> selected<?php endif; ?>><?php _e('Jah', BUUM_TD); ?></option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Impordi värv', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_product[import_color]">
                                <option value=""><?php _e('Ei', BUUM_TD); ?></option>
                                <option value="1"<?php if ($product['import_color']): ?> selected<?php endif; ?>><?php _e('Jah', BUUM_TD); ?></option>
                            </select>
                        </td>
                    </tr>
					<tr>
						<th><?php _e('Impordi suurus', BUUM_TD); ?></th>
						<td>
							<select name="buum_product[import_size]">
								<option value=""><?php _e('Ei', BUUM_TD); ?></option>
								<option value="1"<?php if ($product['import_size']): ?> selected<?php endif; ?>><?php _e('Jah', BUUM_TD); ?></option>
							</select>
						</td>
					</tr>


                    <?php
                    $buumLanguages = $connect->getLanguages();
                    ?>

                    <?php if ( function_exists('icl_object_id') && false): // WPML ?>

                        <?php
                        $languages = apply_filters( 'wpml_active_languages', NULL);
                        foreach ($languages as $langCode => $wpmlLang):
                        ?>

                        <tr>
                            <th><?php echo esc_html($wpmlLang['native_name']); ?></th>
                            <td>
                                <select name="buum_product[name][<?php echo $langCode; ?>]">
                                    <option value=""></option>
                                    <?php foreach ($buumLanguages as $lang): ?>
                                        <option value="<?php echo esc_html($lang['LANGNO']); ?>"<?php if ($product['name'][$langCode] == $lang['LANGNO']): ?> selected<?php endif; ?>><?php echo esc_html($lang['LANGNAME']); ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </td>
                        </tr>

                        <?php endforeach; ?>

                    <?php else: ?>

                    <tr>
                        <th><?php _e('Toote nimetuse väli', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_product[name][default]">
                                <option value=""></option>
                                <?php foreach ($buumLanguages as $lang): ?>
                                <option value="<?php echo esc_html($lang['LANGNO']); ?>"<?php if ($product['name']['default'] == $lang['LANGNO']): ?> selected<?php endif; ?>><?php echo esc_html($lang['LANGNAME']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>

                    <?php endif; ?>

					<tr>
						<th><?php _e( 'Import ARTNO product only', BUUM_TD ); ?></th>
						<td>
							<select name="buum_product[import_artno_only]">
								<option value="">No</option>
								<option value="1" <?php if ($product['import_artno_only']) echo 'selected'?>>Yes</option>
							</select>
						</td>
					</tr>

                </table>
            </div>

            <div class="buum-setting-block">
                <h2><?php _e('Ladude seaded', BUUM_TD); ?></h2>
                <table class="form-table">
                    <tr>
                        <th><?php _e('Arvesta ladusid', BUUM_TD); ?></th>
                        <td>
                            <select style="height: 200px;" multiple name="buum_stock[warehouses][]">
                                <?php foreach ($connect->getWarehouses() as $warehouse): ?>
                                <option value="<?php echo esc_html($warehouse['STOCKNO']); ?>"<?php if (in_array($warehouse['STOCKNO'], $buum_stock['warehouses'])): ?> selected<?php endif; ?>><?php echo esc_html($warehouse['STOCKNAME']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Ladu arvel', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_invoice[default_stock]">
                                <?php foreach ($connect->getWarehouses() as $warehouse): ?>
                                    <option value="<?php echo esc_html($warehouse['STOCKNO']); ?>"<?php if ($warehouse['STOCKNO'] == $buum_invoice['default_stock']): ?> selected<?php endif; ?>><?php echo esc_html($warehouse['STOCKNAME']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="buum-setting-block">
                <h2><?php _e('Klientide seaded', BUUM_TD); ?></h2>
                <table class="form-table">
                    <tr>
                        <th><?php _e('Impordi kliendid gruppidest', BUUM_TD); ?></th>
                        <td>
                            <select style="height: 200px;" multiple name="buum_customer[group_ids][]">
                                <?php foreach ($connect->getCustomerGroups() as $customerGroup): ?>
                                <option value="<?php echo esc_html($customerGroup['custgroupno']); ?>"<?php if (in_array($customerGroup['custgroupno'], $buum_customer['group_ids'])): ?> selected<?php endif; ?>><?php echo esc_html($customerGroup['custgroupname']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Vaikimisi kliendigrupp', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_customer[default_group]">
                                <?php foreach ($connect->getCustomerGroups() as $customerGroup): ?>
                                    <option value="<?php echo esc_html($customerGroup['custgroupno']); ?>"<?php if ($customerGroup['custgroupno'] == $buum_customer['default_group']): ?> selected<?php endif; ?>><?php echo esc_html($customerGroup['custgroupname']); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th><?php _e('Kliendikoodi eesliide', BUUM_TD); ?></th>
                        <td><input type="text" name="buum_customer[prefix]" value="<?php echo @esc_html($buum_customer['prefix']); ?>" /></td>
                    </tr>
                </table>
            </div>

            <div class="buum-setting-block">
                <h2><?php _e('Arvete seaded', BUUM_TD); ?></h2>
                <table class="form-table">
                    <tr>
                        <th><?php _e('Arve eesliide', BUUM_TD); ?></th>
                        <td><input type="text" name="buum_invoice[prefix]" value="<?php echo @esc_html($buum_invoice['prefix']); ?>" /></td>
                    </tr>
                    <tr>
                        <th><?php _e('Arve tüüp', BUUM_TD); ?></th>
                        <td>
                            <select name="buum_invoice[type]">
                                <?php foreach ($connect->getInvoiceTypes() as $invoiceType => $invoiceTypeName): ?>
                                    <option value="<?php echo esc_html($invoiceType); ?>"<?php if ($invoiceType == $buum_invoice['type']): ?> selected<?php endif; ?>><?php echo esc_html($invoiceTypeName); ?></option>
                                <?php endforeach; ?>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="buum-setting-block">
                <h2><?php _e('Tarneviiside artiklid', BUUM_TD); ?></h2>
                <table class="form-table">
                    <?php foreach ($this->get_all_shipping_instances() as $instanceId => $name): ?>
                    <tr>
                        <th><?php echo esc_html($name); ?></th>
                        <td>
                            <input type="text" name="buum_invoice[shipping_article][<?php echo esc_attr($instanceId); ?>]" value="<?php echo @esc_attr($buum_invoice['shipping_article'][$instanceId]); ?>" />
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </table>
            </div>

				<div class="buum-setting-block">
					<h2><?php _e( 'Logs Settings', BUUM_TD ); ?></h2>
					<table class="form-table">
						<tr>
							<th><?php _e( 'Säilitusaeg', BUUM_TD ); ?></th>
							<td>
								<select name="buum_logs_time">
									<?php
									foreach ( $this->time_schedules as $time => $info ) {
										?>
										<option value="<?php esc_attr_e( $time ); ?>"
											<?php echo $time === $logs_time ? 'selected' : ''; ?>>
											<?php esc_html_e( $info['display'] ); ?>
										</option>
										<?php
									}
									?>
								</select>
							</td>
						</tr>
					</table>
				</div>

            <?php else: ?>

            <?php if ($error = getBuum()->getDatabaseError()): ?>
                <div style="color: red; font-weight: bold; margin: 10px 0;"><?php echo esc_html($error); ?></div>
            <?php endif; ?>


            <?php endif; ?>

            <style>
                .buum-setting-block {
                    font-size: 12px;
                    padding: 20px;
                    background: #fff;
                    margin-bottom: 30px;
                }
            </style>

            <?php
        }

        public function process_admin_custom_options() {
            if (isset($_POST['buum_database'])) {
                if ($_POST['buum_database']['pass'] == '****') {
                    $old = get_option('buum_database');
                    $_POST['buum_database']['pass'] = $old['pass'];
                }
                update_option('buum_database', $_POST['buum_database']);
            }
            if (isset($_POST['buum_product'])) {
                update_option('buum_product', $_POST['buum_product']);
            }
            if (isset($_POST['buum_stock'])) {
                update_option('buum_stock', $_POST['buum_stock']);
            }
            if (isset($_POST['buum_customer'])) {
                update_option('buum_customer', $_POST['buum_customer']);
            }
            if (isset($_POST['buum_invoice'])) {
                update_option('buum_invoice', $_POST['buum_invoice']);
            }

			if ( isset( $_POST['buum_logs_time'] ) ) {
				update_option( 'buum_logs_time', $_POST['buum_logs_time'] );
				wp_clear_scheduled_hook( $this->cron_logs_clear_action, array() );
			}
        }

        /**
         * Init
         */
        public function init() {
            if (isset($_GET['test_import_buum_product'])) {
                $this->import_products();
                echo 'done';
                die;
            }
            if (isset($_GET['test_import_buum_customer'])) {
                $this->import_customers();
                echo 'done';
                die;
            }
            if (isset($_GET['test_export_buum_invoice'])) {
                $this->export_invoices($_GET['test_export_buum_invoice']);
                echo 'done';
                die;
            }
			if ( isset( $_GET['test_stock_sync'] ) ) {
				$this->sync_products_stock();
				echo 'done';
				die();
			}
        }

        /**
         * Load integration options
         */
        public function load_options() {
            $this->enabled               = $this->get_option( 'enabled', 'no' ) == 'yes';
            $this->api_url               = $this->get_option( 'api_url', '' );
            $this->cron_frequency        = (int) $this->get_option( 'cron_frequency', 60 );
            $this->cron_enabled          = $this->get_option( 'cron_enabled', 'no' ) == 'yes';
        }

        /**
         * Setup integration settings html
         */
        public function init_form_fields() {
            $this->form_fields = array(
                'enabled' => array(
                    'title'              => 'Liidestus',
                    'type'               => 'checkbox',
                    'label'              => 'Aktiivne',
                    'default'            => 'no',
                ),
            );
        }

        /**
         * Schedule cron hook for automatic integration
         *
         * @return void
         */
        public function schedule_cron() {
            // Set variables
            $this->load_options();
			$logs_time = get_option('buum_logs_time');

            $params   = array( );

            // Recheck
            if( $this->enabled === true ) {
                if (! wp_next_scheduled ( $this->cron_schedule_product_import )) {
                    wp_schedule_event( time(), self::CRON_FIVE_MINUTES, $this->cron_product_import_action );
                }
                if (! wp_next_scheduled ( $this->cron_schedule_customer_import )) {
                    wp_schedule_event( time(), self::CRON_FIVE_MINUTES, $this->cron_customer_import_action );
                }
                if (! wp_next_scheduled ( $this->cron_schedule_invoice_export )) {
                    wp_schedule_event( time(), self::CRON_FIVE_MINUTES, $this->cron_invoice_export_action );
                }

				if ( ! wp_next_scheduled( ( $this->cron_schedule_logs_clear ) ) ) {
					wp_schedule_event( time(), $logs_time, $this->cron_logs_clear_action );
				}

				if ( ! wp_next_scheduled( $this->cron_stock_sync ) ) {
					wp_schedule_event( time(), self::CRON_FIVE_MINUTES, $this->cron_stock_sync_action );
				}
            }
            else {
                wp_clear_scheduled_hook( $this->cron_product_import_action, $params );
                wp_clear_scheduled_hook( $this->cron_customer_import_action, $params );
                wp_clear_scheduled_hook( $this->cron_invoice_export_action, $params );
				wp_clear_scheduled_hook( $this->cron_logs_clear_action, $params );
				wp_clear_scheduled_hook( $this->cron_stock_sync_action, $params );
            }
        }

        /**
         * Do product import cron job
         */
        public function cron_product_import() {
            if(!$this->enabled) {
                return false;
            }
            $this->import_products();
        }

		/**
		 * Do clearing logs table cron job
		 */
		public function cron_logs_clear() {
			if ( ! $this->enabled ) {
				return false;
			}

			$this->logs->clear();
			return true;
		}

        /**
         * Do customer import cron job
         */
        public function cron_customer_import() {
            if(!$this->enabled) {
                return false;
            }
            $this->import_customers();
        }

        /**
         * Do invoice export cron job
         */
        public function cron_invoice_export() {
            if(!$this->enabled) {
                return false;
            }
            $this->export_invoices();
        }

		/**
		 * Do products stock sync cron job
		 */
		public function cron_stock_sync() {
			if ( ! $this->enabled ) {
				return false;
			}
			$this->sync_products_stock();
		}

		public function sync_products_stock() {
			$stock_sync = new \Buum\StockSync();
			$stock_sync->sync( $this->logs );
		}

        /**
         * Do product import
         */
        public function import_products() {
            $importer = new \Buum\ProductImporter();
			$importer->sync( $this->logs );
        }

        /**
         * Do customer import
         */
        public function import_customers() {
            $importer = new Buum_Customer_Importer();
            $importer->sync();
        }

        /**
         * Do invoice export
         */
        public function export_invoices($id = null) {
            $exporter = new Buum_Invoice_Export();
            if ($id) {
                return $exporter->sendInvoice($id);
            }
            $exporter->sync();
        }

        public function get_all_shipping_instances() {
            $instances = array();
            $zones = array();


            $zone                                              = new \WC_Shipping_Zone(0);
            $zones[$zone->get_id()]                            = $zone->get_data();
            $zones[$zone->get_id()]['formatted_zone_location'] = $zone->get_formatted_location();
            $zones[$zone->get_id()]['shipping_methods']        = $zone->get_shipping_methods();

            $shipping_zones = array_merge( $zones, WC_Shipping_Zones::get_zones() );

            foreach ( $shipping_zones as $shipping_zone ) {

                foreach ( $shipping_zone['shipping_methods'] as $sm_obj ) {
                    $method_id   = $sm_obj->id;
                    $instance_id = $sm_obj->get_instance_id();
                    $enabled = $sm_obj->is_enabled() ? true : 0;

                    $instances[$sm_obj->get_rate_id()] = $sm_obj->get_title();


                }
            }

            foreach (WC()->shipping()->get_shipping_methods() as $id => $sm_obj) {
                if (in_array($id, array('flat_rate', 'free_shipping', 'local_pickup'))) {
                    //continue;
                }
                if (!isset($instances[$sm_obj->get_rate_id()])) {
                    $instances[$sm_obj->get_rate_id()] = $sm_obj->get_title() ? $sm_obj->get_title() : $sm_obj->method_title;
                }
            }

            return $instances;
        }

		public function create_variation_buum_id_field( $loop, $variation_data, $variation ) {
			$buum_id = get_post_meta( $variation->ID, '_buum_id', true );
			echo '<div class="form-row form-row-first">';
			woocommerce_wp_text_input( array(
				'id'    => '_buum_id[' . $loop . ']',
				'class' => 'short',
				'label' => __( 'Buum ID (ARTNO)', BUUM_TD ),
				'value' => $buum_id,
			) );
			echo '</div>';
		}

		public function update_variation_buum_id_field( $variation_id, $i ) {
			$id = isset( $_POST['_buum_id'][ $i ] ) ? $_POST['_buum_id'][ $i ] : '';
			update_post_meta( $variation_id, '_buum_id', esc_attr( $id ) );
		}

		public function create_buum_id_field() {
			global $post;
			$buum_id = get_post_meta( $post->ID, '_buum_id', true );
			?>

			<p class="form-row form-field" id="buum_id_field" data-priority="">
				<label for="buum_id"><?php esc_html_e( 'Buum ID (ARTNO)', BUUM_TD ); ?></label>
				<span class="woocommerce-input-wrapper">
					<input type="text"
						   class="input-text short"
						   name="_buum_id_field"
						   id="_buum_id_field"
						   placeholder=""
						   value="<?php esc_html_e( $buum_id ); ?>"/>
				</span>
			</p>
			<?php
		}

		public function update_buum_id_field( $id ) {
			$product = wc_get_product( $id );
			$id      = isset( $_POST['_buum_id_field'] ) ? $_POST['_buum_id_field'] : '';

			$product->update_meta_data( '_buum_id', $id );
			$product->save();
		}
    }



endif;