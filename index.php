<?php
/**
 * Plugin Name: BUUM integration
 * Plugin URI: http://www.ids.ee/wp-buum
 * Version: 1.26
 * Author: iDS
 * Author URI: http://www.ids.ee
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

/**
 * Plugin path
 */
define( 'BUUM_INTEGRATION_PATH', dirname( __FILE__ ) . '/' );

/**
 * Plugin textdomain
 */
define( 'BUUM_TD', 'buum');

/**
 * Class Buum_Integration
 */
class Buum_Integration {

    /**
     * Buum connection
     *
     * @var Buum_Connect
     */
    protected $connect = null;

    protected $database_error = null;

    /**
     * Directo_Importer_Vallin constructor.
     */
    function __construct() {
        require_once BUUM_INTEGRATION_PATH . '/includes/buum-connect.php';
        require_once BUUM_INTEGRATION_PATH . '/includes/customer-importer.php';
        require_once BUUM_INTEGRATION_PATH . '/includes/price-calculator.php';
        require_once BUUM_INTEGRATION_PATH . '/includes/invoice-export.php';

		require_once plugin_dir_path( __FILE__ ) . 'autoloader.php';
		$loader = new \Psr4AutoloaderClass();
		$loader->register();
		$loader->addNamespace( 'Buum', __DIR__ . '/includes' );

        new Buum_Price_Calculator();

        try {
            $params = get_option('buum_database');
            if (isset($params['host'])) {
                $connect = new Buum_Connect($params['host'], $params['db'], $params['user'], $params['pass']);
                $this->connect = $connect;
            }
        }
        catch (Exception $e) {
            $this->database_error = $e->getMessage();
        }
        add_action( 'init',           array( $this, 'init' ) );
        add_action( 'plugins_loaded', array( $this, 'plugins_loaded' ) );
    }

    public function init() {

    }

    /**
     * Buum connection
     *
     * @return Buum_Connect
     */
    public function getConnection() {
        return $this->connect;
    }

    public function getDatabaseError() {
        return $this->database_error;
    }

    /**
     * Initialize plugin
     * @return void
     */
    public function plugins_loaded() {
        if (!class_exists( 'WC_Integration' )) {
            return false;
        }

        add_filter( 'woocommerce_integrations', array( $this, 'add_integration' ) );

        // Load functionality
        $this->load_files();
    }

    /**
     * Load requried files
     */
    public function load_files() {
        require_once BUUM_INTEGRATION_PATH . '/includes/wc-integration.php';
    }

    /**
     * Add a new integration to WooCommerce.
     */
    public function add_integration( $integrations ) {
        $integrations[] = 'WC_Buum_Integration';

        return $integrations;
    }



}

global $buum_integration;
$buum_integration = new Buum_Integration();

/**
 * Get Buum main object
 *
 * @return Buum_Integration
 */
function getBuum() {
    global $buum_integration;
    return $buum_integration;
}